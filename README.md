# xz-static RPM

### Purpose

- Builds xz rpms with xz-static
- deployment to [https://repo.deployctl.com](https://repo.deployctl.com)


### INSTALL

```
curl https://repo.deployctl.com/repo.rpm.sh | sudo bash
yum install xz-static xz-devel
```


----

*deployed with [deployctl][https://www.deployctl.com]*
